<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Session.php');
Session::init();

if (isset($_GET['action']) && $_GET['action'] == "logout") {
	Session::destroy();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Online Event Portal</title>

	<!-- Bootstrap -->
	<link href="inc/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Comments -->
	<link href="inc/css/comment.css" rel="stylesheet">

	<!-- Custom stylesheet -->
	<link href="inc/css/style.css" rel="stylesheet">
</head>
<body>

	<div class="container">
		
		<!-- Default Navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
		    	<!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
			      	</button>
			      	<a class="navbar-brand" href="index.php">Online Event Portal</a>
			    </div>

		    	<!-- Collect the nav links, forms, and other content for toggling -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
						<!--
		        		<li class="active"><a href="#">Link 
		        			<span class="sr-only">(current)</span></a>
		        		</li>	
						-->
		      		</ul>
		    
			      	<ul class="nav navbar-nav navbar-right">

<?php
	if(Session::get("login") == true)
	{ ?>
		<li class="dropdown">
        	<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo Session::get('username'); ?><span class="caret"></span></a>
          	<ul class="dropdown-menu">

			<?php
				if(Session::get("role") == 1) { ?>
					<li>
	            		<a href="addEvent.php">
							<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
	            			Add event
	            		</a>
	            	</li> 
					<li>
	            		<a href="participantsLists.php">
							<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> 
	            			Participant's lists
	            		</a>
	            	</li> 
	            	<?php
				}
			?>

            	<li>
            		<a href="eventsToParticipate.php">
						<span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span> 
            			Events
            		</a>
            	</li>
            	<li>
            		<a href="profile.php">
            			<span class="glyphicon glyphicon-edit" aria-hidden="true"></span> 
            			Profile
            		</a>
            	</li>
            	<li role="separator" class="divider"></li>
	            <li>
	            	<a href="?action=logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
	            		Logout
	            	</a>
	            </li>
          	</ul>
		</li> <?php
	}
	else 
	{ ?>
		<li><a href="register.php">Register</a></li>
		<li><a href="login.php">Login</a></li> <?php
	}

?>

			      	</ul>
		    	</div><!-- /.navbar-collapse -->
		  	</div><!-- /.container-fluid -->
		</nav>
