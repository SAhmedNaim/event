<?php

include 'Database.php';
include 'Session.php';

class Event
{
	private $db;

	public function __construct() 
	{
		$this->db = new Database();
	}
	
	public function getEventBySearch($data)
	{
		$area 		= implode(', ', $data['area']);
		$interest 	= implode(', ', $data['interest']);
		
		$sql = "SELECT * FROM event WHERE topic IN (".$interest.") AND location IN (".$area.");";
		$query = $this->db->pdo->prepare($sql);
		$query->execute();
		$result = $query->fetchAll();

		if(!empty($result)) 
		{
			return $result;
		} 
		else {
			header('Location:index.php?data=null');
		}
	}

	public function getEventById($event_id)
	{
		$sql = "SELECT * FROM event WHERE event_id = :event_id LIMIT 1;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':event_id', $event_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function participate($eventId)
	{
		Session::set("participateEventId", $eventId);
		header("Location: congratulation.php");
	}

	public function addEvent($data, $file)
	{
		$title 			=  $data['title'];
		$location 		=  $data['location'];
		$topic 			=  $data['topic'];
		$total_seat 	=  $data['seat'];
		$dates 			=  $data['date'];
		$description 	=  $data['description'];
		
		$user_user_id 	= Session::get('id');

		// image
		$permited  = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
        $uploaded_image = "uploads/".$unique_image;
 
		if($title == "" OR $location == "" OR $topic == "" OR $total_seat == "" OR $dates == "" OR $description == "" OR $file_name == "")
		{
			return '<div class="alert alert-danger alert-dismissible">
  						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  						<strong>Error!</strong> Fields must not be empty!!!
					</div>';
		}
		elseif(in_array($file_ext, $permited) === false)
		{
			return '<div class="alert alert-danger alert-dismissible">
  						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  						<strong>Error!</strong> Image must be !!!' . implode(', ', $permited) . '
					</div>';
		}

		move_uploaded_file($file_temp, $uploaded_image);

		$sql = "INSERT INTO event(title, description, image, dates, total_seat, topic, location, user_user_id) VALUES(:title, :description, :image, :dates, :total_seat, :topic, :location, :user_user_id);";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':title', $title);
		$query->bindValue(':description', $description);
		$query->bindValue(':image', $uploaded_image);
		$query->bindValue(':dates', $dates);
		$query->bindValue(':total_seat', $total_seat);
		$query->bindValue(':topic', $topic);
		$query->bindValue(':location', $location);
		$query->bindValue(':user_user_id', $user_user_id);
		$result = $query->execute();

		if ($result) 
		{
			$msg = "<div class='alert alert-success alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Thank you, Your event has been registered!</div>";
			return $msg;
		} 
		else 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Sorry! There has been problem inserting your event details.</div>";
			return $msg;
		}

	}

}
