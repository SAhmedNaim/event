<?php 
include_once('inc/header.php'); 
include 'lib/User.php';
Session::checkSession();

$user = new User;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
	$updateUser = $user->updateUser($_POST);
}

if (isset($updateUser)) {
	echo $updateUser;
}

if(Session::get('id')) {
	$getUserById = $user->getUserById(Session::get('id'));
}

if(isset($getUserById)) {
	foreach ($getUserById as $user) { ?> 

<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">
			<h2>User profile</h2>
		</div>
	</div>
	<div class="panel-body">
		
		<form action="" method="post">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" id="username" value="<?php echo $user['username']; ?>" name="username" />
			</div>

			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" value="<?php echo $user['email']; ?>" disabled name="email" />
			</div>

			<div class="form-group">
				<label for="role">Role</label>
				<input type="text" class="form-control" id="role" value="
<?php 
						if($user['role'] == 1)
						{
							echo 'Event Manager';
						}
						elseif($user['role'] == 2)
						{
							echo 'General User';
						}
?>
				" disabled name="role" />
			</div>

			<div class="form-group">
				<label for="location">Location</label>
				<select class="form-control" id="location" name="location">
					<option disabled>Select location</option>
					<option value="1" <?php
						if($user['location'] == 1) echo 'selected';
					?> >Dhaka</option>
					<option value="2" <?php
						if($user['location'] == 2) echo 'selected';
					?> >Chittagonj</option>
					<option value="3" <?php
						if($user['location'] == 3) echo 'selected';
					?> >Rajshahi</option>
					<option value="4" <?php
						if($user['location'] == 4) echo 'selected';
					?> >Khulna</option>
					<option value="5" <?php
						if($user['location'] == 5) echo 'selected';
					?> >Barisal</option>
					<option value="6" <?php
						if($user['location'] == 6) echo 'selected';
					?> >Sylhet</option>
					<option value="7" <?php
						if($user['location'] == 7) echo 'selected';
					?> >Mymensingh</option>
				</select>
			</div>

			<div class="form-group">
				<label for="interest">Interest</label>
				<select class="form-control" id="interest" name="interest">
					<option disabled>Select location</option>

					<option value="1" <?php
						if($user['interest'] == 1) echo 'selected';
					?> >Technology</option>
					<option value="2" <?php
						if($user['interest'] == 2) echo 'selected';
					?> >Science</option>
					<option value="3" <?php
						if($user['interest'] == 3) echo 'selected';
					?> >Politics</option>
					<option value="4" <?php
						if($user['interest'] == 4) echo 'selected';
					?> >Sports</option>
					<option value="5" <?php
						if($user['interest'] == 5) echo 'selected';
					?> >Entertainments</option>
					<option value="6" <?php
						if($user['interest'] == 6) echo 'selected';
					?> >Business</option>
					<option value="7" <?php
						if($user['interest'] == 7) echo 'selected';
					?> >Software</option>
				</select>
			</div>

			<!-- 
			<div class="form-group">
			    <label for="exampleInputFile">Image</label>
			    <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
			    <small id="fileHelp" class="form-text text-muted">Browser image here to update your profile picture..</small>
			</div> 
			-->

			<input type="submit" class="btn btn-default" name="update" value="Update" />
		</form>

	</div>
</div> <?php
	}
}


?>

<?php include_once('inc/footer.php'); ?>
