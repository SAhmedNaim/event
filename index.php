<?php 
	include_once('inc/header.php'); 

	if(isset($_GET['field']) AND $_GET['field'] == 'null')
	{ 
		echo '<div class="alert alert-danger alert-dismissible">
  				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  				<strong>Error!</strong> Fields must not be empty!
			</div>';
	}

	if(isset($_GET['data']) AND $_GET['data'] == 'null')
	{ 
		echo '<div class="alert alert-danger alert-dismissible">
  				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  				<strong>Error!</strong> Data not found!
			</div>';
	}
?>

<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">

			<form action="events.php" method="get">

				<div class="col-md-2"></div>
				<div class="col-md-4">
					<div class="area">
						<h3>Select area</h3>
						<hr/>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="1">Dhaka</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="2">Chittagonj</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="3">Rajshahi</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="4">Khulna</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="5">Barisal</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="6">Sylhet</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="area[]" value="7">Mymensingh</label>
					    </div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="interest">
						<h3>Select interest</h3>
						<hr/>
						<div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="1">Technology</label>
					    </div>
						<div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="2">Science</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="3">Politics</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="4">Sports</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="5">Entertainments</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="6">Business</label>
					    </div>
					    <div class="checkbox">
					      	<label><input type="checkbox" name="interest[]" value="7">Software</label>
					    </div>
					</div>
				</div>
				<div class="col-md-2"></div>
		
				<input type="submit" name="submit" class="btn btn-default btn-lg btn-block go" value="GO" />
				
			</form>
		</div>
	</div>

</div>

<?php include_once('inc/footer.php'); ?>