<?php 
include_once('inc/header.php'); 
include 'lib/User.php';

$user = new User;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
	$userRegistration = $user->userRegistration($_POST);
}

if (isset($userRegistration)) {
	echo $userRegistration;
}

?>


<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><h2 align="center">User registration</h2></h3>
	</div>
	<div class="panel-body">
		
		<form action="" method="post">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" class="form-control" name="username" id="username" placeholder="Type username here..." />
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Type email here..." >
			</div>

			<div class="form-group">
				<label for="location">Location</label>
				<select class="form-control" id="location" name="location">
					<option selected value="">Select location</option>
					<option value="1">Dhaka</option>
					<option value="2">Chittagonj</option>
					<option value="3">Rajshahi</option>
					<option value="4">Khulna</option>
					<option value="5">Barisal</option>
					<option value="6">Sylhet</option>
					<option value="7">Mymensingh</option>
				</select>
			</div>

			<div class="form-group">
				<label for="interest">Interest</label>
				<select class="form-control" id="interest" name="interest">
					<option selected value="">Select interest</option>
					<option value="1">Technology</option>
					<option value="2">Science</option>
					<option value="3">Politics</option>
					<option value="4">Sports</option>
					<option value="5">Entertainments</option>
					<option value="6">Business</option>
					<option value="7">Software</option>
				</select>
			</div>

			<div class="form-group">
				<label for="role">User role</label>
				<select class="form-control" id="role" name="role">
					<option selected value="">Select user role</option>
					<option value="1">Event manager</option>
					<option value="2">General user</option>
				</select>
			</div>

			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Type password here..." >
			</div>

			<!-- <button type="button" class="btn btn-default">Submit</button> -->
			<input type="submit" name="register" class="btn btn-default"/>

		</form>

	</div>
</div>

<?php include_once('inc/footer.php'); ?>
