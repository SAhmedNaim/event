<?php 
include 'inc/header.php'; 
include 'lib/Participant.php';
include 'lib/Session.php';
Session::checkSession();

if(Session::get('participateEventId') == null)
{
	header('Location:index.php');
}
else
{
	$participant = new Participant;
	$token = $participant->participateToEvent();
}

?>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="congratulation">
					<h2 align="center">Congratulation</h2>
					<hr/>
				    <h3 align="justify">
						We are very glad that you are participating in our event. Your valid token: <?php echo $token; ?>
				    </h3>
				</div>
			</div>
			<div class="col-md-2"></div>
		
		</div>
	</div>
</div>

<?php include_once('inc/footer.php'); ?>