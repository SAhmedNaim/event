<?php 

include_once('inc/header.php'); 
include 'lib/Session.php';
Session::checkSession();

include 'lib/Participant.php';

if(Session::get('role') != 1) {
	header('Location:index.php');
}

$participant = new Participant;
$getEventsByUser = $participant->getEventsByUser();

// echo count($getEventsByUser);

if(!empty($getEventsByUser)) {
	foreach ($getEventsByUser as $events) { ?>
		<!-- //echo $events['event_event_id']; -->

<div class="panel panel-default">

	<div class="panel-heading">
		<div class="panel-title">
			<h3>Participant's lists: <a href="event.php?id=<?php echo $events['event_id']; ?>">
				<?php echo $events['title']; ?></a>
				<span class="pull-right">
					Total seat: <?php echo $events['total_seat']; ?>
				</span>
			</h3>
		</div>
	</div>

	<div class="panel-body">

		<table class="table table-striped table-bordered table-hover">
		    <thead>
		    	<tr>
		        	<th width="10%">Seat</th>
		        	<th width="20%">Name</th>
		        	<th width="25%">Email</th>
		        	<th width="15%">Location</th>
		        	<th width="30%">Token</th>
		      	</tr>
		    </thead>

		    <tbody>

			<?php 
				$getUserDetails = $participant->getUserDetails($events['event_id']);
				if($getUserDetails) {
					$seat = 1;
					foreach($getUserDetails as $userDetails) { ?>
				      	<tr>
				        	<td><?php echo $seat++; ?></td>
				        	<td><?php echo $userDetails['username']; ?></td>
				        	<td><?php echo $userDetails['email']; ?></td>

				        	<td>

				        	<?php
				        		if($userDetails['location'] == 1) echo 'Dhaka';
				        		elseif($userDetails['location'] == 2) echo 'Chittagonj';
				        		elseif($userDetails['location'] == 3) echo 'Rajshahi';
				        		elseif($userDetails['location'] == 4) echo 'Khulna';
				        		elseif($userDetails['location'] == 5) echo 'Barisal';
				        		elseif($userDetails['location'] == 6) echo 'Sylhet';
				        		elseif($userDetails['location'] == 7) echo 'Mymensingh';
				        	?>

				        	</td>

				        	<td><?php echo $userDetails['token']; ?></td>
				      	</tr> <?php
					}
				}
			?>

		    </tbody>

		</table>

	</div>
</div> <?php
	}
} 
else { ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<h2 align="center">Participant's lists</h2>
			</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<h3 class="breadcrumb">
						Data not found!!!	
					</h3>
				</div>
			</div>
		</div>
	</div> <?php
}

?>

<?php include_once('inc/footer.php'); ?>