<?php 

include_once('inc/header.php'); 
include('lib/User.php'); 

$user = new User;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
	$userLogin = $user->userLogin($_POST);
}

if (isset($userLogin)) {
	echo $userLogin;
}

?>

<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title"><h2 align="center">User login</h2></h3>
	</div>

	<div class="panel-body">
		
		<form action="" method="post">
			<div class="form-group">
				<label for="email">Email address:</label>
				<input type="text" class="form-control" id="email" name="email" placeholder="Type email here..." />
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Type password here..." >
			</div>
			<!-- <button type="button" class="btn btn-default" onclick="window.location.href='congratulation.php'">Submit</button> -->
			<input type="submit" class="btn btn-default" name="login"/>
		</form>

	</div>
</div>

<?php include_once('inc/footer.php'); ?>
