<?php 
	include_once('inc/header.php'); 
	include 'lib/Event.php';

	$event = new Event;

	if(isset($_GET['submit']) AND isset($_GET['area']) AND isset($_GET['interest']))
	{
		$events = $event->getEventBySearch($_GET);
	}
	else 
	{
		header('Location:index.php?field=null');
	}

if(isset($events)) {
	foreach($events as $event) { ?>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h2 class="breadcrumb"><?php echo $event['title']; ?></h2>
						<p class="text-justify"><?php echo $event['description']; ?></p>
						<a href="event.php?id=<?php echo $event['event_id']; ?>" class="btn btn-default view-details">View details</a>
					</div>
				</div>
			</div> 
		</div> <?php
	}
}

?>

<?php include_once('inc/footer.php'); ?>