<?php 

include_once('inc/header.php');
include 'lib/Event.php';

$event = new Event;

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['addEvent'])) {

	// echo $_POST['title'] . "<br/>";
	// echo $_POST['location'] . "<br/>";
	// echo $_POST['topic'] . "<br/>";
	// echo $_POST['seat'] . "<br/>";
	// echo $_POST['date'] . "<br/>";
	// echo $_POST['description'] . "<br/>";
	// echo $_POST['image'] . "<br/>";

	$addEvent = $event->addEvent($_POST, $_FILES);
}

if(isset($addEvent)) {
	echo $addEvent;
}

?>

<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">
			<h2>Add event information</h2>
		</div>
	</div>
	<div class="panel-body">
		
		<form action="" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">Event title</label>
				<input type="text" class="form-control" name="title" id="title" placeholder="Type event title here..." />
			</div>
			
			<div class="form-group">
				<label for="location">Location</label>
				<select class="form-control" id="location" name="location">
					<option value="" selected>Select location</option>
					<option value="1">Dhaka</option>
					<option value="2">Chittagonj</option>
					<option value="3">Rajshahi</option>
					<option value="4">Khulna</option>
					<option value="5">Barisal</option>
					<option value="6">Sylhet</option>
					<option value="7">Mymensingh</option>
				</select>
			</div>

			<div class="form-group">
				<label for="topic">Topic</label>
				<select class="form-control" id="topic" name="topic">
					<option value="" selected>Select topic</option>
					<option value="1">Technology</option>
					<option value="2">Science</option>
					<option value="3">Politics</option>
					<option value="4">Sports</option>
					<option value="5">Entertainments</option>
					<option value="6">Business</option>
					<option value="7">Software</option>
				</select>
			</div>

			<div class="form-group">
				<label for="seat">Total seat capicity</label>
				<input type="number" class="form-control" id="seat" name="seat" placeholder="Type total seat capacity here..." />
			</div>

			<div class="form-group">
				<label for="date">Date</label>
				<input type="date" class="form-control" id="date" name="date" placeholder="Type date here..." />
			</div>
			
			<div class="form-group">
				<label for="description">Description</label>
				 <textarea class="form-control" rows="5" id="description" name="description" placeholder="Type description here..."></textarea>
			</div>

			<div class="form-group">
			    <label for="image">Add event image</label>
			    <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp">
			    <small id="fileHelp" class="form-text text-muted">Browser image here to set your event thumbnail..</small>
				</div>

			<input type="submit" name="addEvent" class="btn btn-default" value="Create event"/>

			<!-- <button type="button" class="btn btn-default">Create event</button> -->
		</form>

	</div>
</div>

<?php include_once('inc/footer.php'); ?>