<?php

include 'Session.php';
include 'Database.php';

/**
* 
*/
class Participant
{
	private $db;

	public function __construct() 
	{
		$this->db = new Database();
	}

	public function participateToEvent()
	{
		$userId = Session::get('id');
		$eventId = Session::get('participateEventId');
		$token = md5(Session::get('id').Session::get('participateEventId'));

		//echo $userId . " " . $eventId . " " . $token;


		$sql = "INSERT INTO participant(user_user_id, event_event_id, token) VALUES(:userId, :eventId, :token);";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':userId', $userId);
		$query->bindValue(':eventId', $eventId);
		$query->bindValue(':token', $token);
		$result = $query->execute();

		unset($_SESSION['participateEventId']);

		return $token;
	}

	public function eventsToParticipate()
	{
		$user_user_id = Session::get('id');

		$sql = "SELECT * FROM participant 
				JOIN event 
				ON 
				participant.event_event_id = event.event_id
				AND participant.user_user_id  = :user_user_id";
				
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':user_user_id', $user_user_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function getEventsByUser()
	{
		$user_user_id = Session::get('id');

		// $sql = "SELECT DISTINCT participant.event_event_id, event.* FROM participant JOIN event ON participant.event_event_id = event.event_id AND participant.user_user_id = :user_user_id ORDER BY dates DESC;";

		$sql = "SELECT * FROM event WHERE user_user_id = :user_user_id;";

		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':user_user_id', $user_user_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function getUserDetails($event_event_id)
	{
		$sql = "SELECT * FROM participant JOIN user ON participant.user_user_id = user.user_id AND participant.event_event_id = :event_event_id;";

		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':event_event_id', $event_event_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}


}