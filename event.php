<?php 

include_once('inc/header.php'); 
include 'lib/Event.php';
include 'lib/Comment.php';

$comment = new Comment;

$event = new Event;
if (isset($_GET['id'])) {
	$getEventById = $event->getEventById($_GET['id']);
}

if(empty($getEventById)) {
	header('Location:index.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['participate'])) {
	$participate = $event->participate($_GET['id']);
}

if(isset($_POST['makeComment'])) {
	$addUserComment = $comment->addUserComment($_POST);
}

if(isset($_POST['makeReply'])) {
	$addUserComment = $comment->addUserReply($_POST);
}

?>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12 event-details">

				<?php
					if(isset($getEventById)) { 
						foreach ($getEventById as $data) { ?>
							<h2 class="breadcrumb text-center"><?php echo $data['title']; ?></h2>
							<img src="<?php echo $data['image']; ?>" alt="Event image missing" />
							<p class="text-justify"><?php echo $data['description']; ?></p> <?php
						}
					}
				?>

				<!-- <button type="button" class="btn btn-default btn-lg btn-block" value="participate">Participate
				</button> -->
				
				<form action="" method="post">
					<input type="submit" class="btn btn-default btn-lg btn-block" name="participate" value="Participate" />
				</form>
				

				<!-- Comments -->
				<div class="comment">
					<label for="comment" class="commentLabel"><h4>Comments</h4></label>
					<div class="input-group comment-box">
					
					<?php
					if(Session::get("login") == true)
					{ ?>


					<form action="" method="post">
						<input type="text" class="form-control comment-box" placeholder="Type your comment..." aria-describedby="comment" name="comment" id="comment" style="width:82%;">

						<input type="hidden" name="user_user_id" value="<?php echo Session::get('id'); ?>">

						<input type="hidden" name="event_event_id" value="<?php echo $_GET['id']; ?>">
						

						<!-- <span class="input-group-addon btn btn-default comment-box">Comment</span> -->
						<input type="submit" style="width:20%; margin: -56px 0px auto 890px; height: 34px;" class="input-group-addon btn btn-default comment-box" value="Comment" name="makeComment">
					</form> <?php 
					}
					?>

					</div>
					
					<!--
					<div class="row comment-box">
						<div class="col-md-1">
							<div class="thumbnail">
								<img class="img-responsive user-photo" src="inc/img/avatar.png">
							</div>
						</div>
						<div class="col-md-11">
							<div class="panel panel-default">
								<div class="panel-heading">
									<strong>Donald Trump</strong> <span class="text-muted pull-right">27-Mar-18 11:50:27pm</span>
								</div>
								<div class="panel-body">
									I am very much interested of this event...
								</div>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-sm-1">
							<div class="thumbnail">
								<img class="img-responsive user-photo" src="inc/img/avatar.png">
							</div>
						</div>
						<div class="col-md-11">
							<div class="panel panel-default">
								<div class="panel-heading">
									<strong>Barack Obama</strong> <span class="text-muted pull-right">27-Mar-18 11:53:21pm</span>
								</div>
								<div class="panel-body">
									I am comming only for attend to this event from U.S.A!!!
								</div>
							</div>
						</div>
					</div>
					-->



<!-- loop start -->

<?php

$getCommentsByEvent = $comment->getCommentsByEvent($_GET['id']);

if($getCommentsByEvent) {
	foreach ($getCommentsByEvent as $comments) {
		if($comments['comment_id'] == 0) { ?>
								
					
<br>
<!-- Comment loop start -->
<div class="row">
	<div class="col-sm-1">
		<div class="thumbnail">
			<img class="img-responsive user-photo" src="inc/img/avatar.png">
		</div><!-- /thumbnail -->
	</div><!-- /col-sm-1 -->
	<div class="col-md-11">
		<div class="panel panel-default">
			<div class="panel-heading">
				<strong><?php echo $comments['username']; ?></strong> <span class="text-muted pull-right"><?php echo date('F j, Y, g:i a', strtotime($comments['time'])); ?></span>
			</div>
			<div class="panel-body">
				<?php echo $comments['comment']; ?>
			</div><!-- /panel-body -->
		</div><!-- /panel panel-default -->
		
		<!-- input field for reply of the comment -->
		<!-- <div class="input-group">
			<input type="text" class="form-control" placeholder="Type your comment..." aria-describedby="comment" name="comment" id="comment">
			<span class="input-group-addon btn btn-default">Reply</span>
		</div> -->

		<form action="" method="post">
			<input type="text" class="form-control comment-box" placeholder="Type your comment..." aria-describedby="reply" name="reply" id="reply" style="width:82%;">

			<input type="hidden" name="user_user_id" value="<?php echo Session::get('id'); ?>">
			<input type="hidden" name="event_event_id" value="<?php echo $_GET['id']; ?>">
			<input type="hidden" name="comment_id" value="<?php echo $comments['id']; ?>">
			

			<!-- <span class="input-group-addon btn btn-default comment-box">Comment</span> -->
			<input type="submit" style="width:18%; margin: -56px 0px auto 831px; height: 34px;" class="input-group-addon btn btn-default comment-box" value="Reply" name="makeReply">
		</form> 


	</div><!-- /col-sm-5 -->
</div><!-- /row -->

<br>
<!-- 1st reply -->

<?php

$replies = $comment->getReplyByEvent($comments['id']);

if($replies) {
	foreach ($replies as $reply) {
		if($reply['comment_id'] != 0) { ?>

			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-1">
					<div class="thumbnail">
						<img class="img-responsive user-photo" src="inc/img/avatar.png">
					</div><!-- /thumbnail -->
				</div><!-- /col-sm-1 -->
				<div class="col-md-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<strong><?php echo $reply['username']; ?></strong> <span class="text-muted pull-right"><?php echo date('F j, Y, g:i a', strtotime($reply['time'])); ?></span>
						</div>
						<div class="panel-body">
							<?php echo $reply['comment']; ?>
						</div><!-- /panel-body -->
					</div><!-- /panel panel-default -->
				</div><!-- /col-sm-5 -->
			</div><!-- /row -->



			<?php 
		} 
	} 
}

		}
	}
}
?>

					
					<!-- loop end -->

					
					
					<!--
					<div class="row">
						<div class="col-sm-1">
							<div class="thumbnail">
								<img class="img-responsive user-photo" src="inc/img/avatar.png">
							</div>
						</div>
						<div class="col-md-11">
							<div class="panel panel-default">
								<div class="panel-heading">
									<strong>Elon Mask</strong> <span class="text-muted pull-right">27-Mar-18 11:58:48pm</span>
								</div>
								<div class="panel-body">
									Absolutely perfect
								</div>
							</div>
						</div>
					</div>
					-->

				</div>

			</div>
		</div>
	</div>
</div>

<?php include_once('inc/footer.php'); ?>
