<?php 
include_once('inc/header.php'); 
include 'lib/Participant.php';
include 'lib/Session.php';

Session::checkSession();

$participant = new Participant;
$eventsToParticipate = $participant->eventsToParticipate();

if(!empty($eventsToParticipate)) {
	foreach ($eventsToParticipate as $events) { ?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<h2 align="center">Events to participate</h2>
				</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb">
							<?php echo $events['title']; ?>
							<span class="pull-right">Date: 
								<?php echo date('F j, Y, g:i a', strtotime($events['dates'])); ?>
							</span>
						</h3>
						<p class="text-justify"><?php echo $events['description']; ?></p>
						<a href="event.php?id=<?php echo $events['event_id']; ?>" class="btn btn-default view-details">View details</a>
					</div>
				</div>
			</div>
		</div> <?php

	}
} else { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					<h2 align="center">Events to participate</h2>
				</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb">
							Data not found!!!	
						</h3>
					</div>
				</div>
			</div>
		</div> <?php
}

?>

<?php include_once('inc/footer.php'); ?>