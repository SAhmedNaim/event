<?php

include 'Session.php';

/**
* 
*/
class Comment
{
	private $db;

	public function __construct() 
	{
		$this->db = new Database();
	}

	public function getCommentsByEvent($event_event_id)
	{
		$sql = "SELECT * FROM comments JOIN user on comments.user_user_id = user.user_id AND event_event_id = :event_event_id ORDER BY time ASC;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':event_event_id', $event_event_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function getReplyByEvent($comment_id)
	{
		$sql = "SELECT * FROM comments JOIN user on comments.user_user_id = user.user_id AND comment_id = :comment_id ORDER BY time ASC;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':comment_id', $comment_id);
		$query->execute();
		$result = $query->fetchAll();
		return $result;
	}

	public function addUserComment($data)
	{
		$comment = $data['comment'];
		$user_user_id = $data['user_user_id'];
		$event_event_id = $data['event_event_id'];

		if($comment == "") 
		{
			return "";
		}
		else 
		{
			$sql = "INSERT INTO comments(comment, user_user_id, event_event_id) VALUES(:comment, :user_user_id, :event_event_id);";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':comment', $comment);
			$query->bindValue(':user_user_id', $user_user_id);
			$query->bindValue(':event_event_id', $event_event_id);
			$result = $query->execute();
		}
	}

	public function addUserReply($data)
	{
		$comment 		= $data['reply'];
		$user_user_id 	= $data['user_user_id'];
		$event_event_id = $data['event_event_id'];
		$comment_id 	= $data['comment_id'];

		if($comment == "") 
		{
			return "";
		}
		else 
		{
			$sql = "INSERT INTO comments(comment, user_user_id, event_event_id, comment_id) VALUES(:comment, :user_user_id, :event_event_id, :comment_id);";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':comment', $comment);
			$query->bindValue(':user_user_id', $user_user_id);
			$query->bindValue(':event_event_id', $event_event_id);
			$query->bindValue(':comment_id', $comment_id);
			$result = $query->execute();
		}
	}
	
}



