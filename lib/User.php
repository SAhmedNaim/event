<?php

include 'Session.php';
include 'Database.php';

class User
{
	private $db;

	public function __construct() 
	{
		$this->db = new Database();
	}

	public function emailCheck($email) 
	{
		$sql = "SELECT EMAIL FROM user WHERE email = :email";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->execute();
		if ($query->rowCount() > 0) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	public function userRegistration($data) 
	{
		$username 	= $data['username'];
		$email 		= $data['email'];
		$location 	= $data['location'];
		$interest 	= $data['interest'];
		$role		= $data['role'];
		$password 	= $data['password'];

		if(strlen($username) < 3) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Username must not less than 3 digits!</div>";
			return $msg;
		}

		if(strlen($password) < 5) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Username must not less than 3 digits!</div>";
			return $msg;
		}

		$chk_email	= $this->emailCheck($email);

		if ($username == "" OR $email == "" OR $location == "" OR $interest == "" OR $role == "" OR $password == "") 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Field must not be empty!</div>";
			return $msg;
		}

		//if(preg_match('/[^a-z0-9]+/i', $password))
		if (ctype_alnum($password))
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Password must be alphanumerical, dashes and underscore!!</div>";
			return $msg;
		}

		$password 	= md5($data['password']);

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Email address is not valid!</div>";
			return $msg;
		}

		if ($chk_email == true) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Email address already exists!</div>";
			return $msg;
		}

		$sql = "INSERT INTO user(username, email, role, location, interest, password) VALUES(:username, :email, :role, :location, :interest, :password);";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':username', $username);
		$query->bindValue(':email', $email);
		$query->bindValue(':role', $role);
		$query->bindValue(':location', $location);
		$query->bindValue(':interest', $interest);
		$query->bindValue(':password', $password);
		$result = $query->execute();

		if ($result) 
		{
			$msg = "<div class='alert alert-success alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Thank you, You have been registered.</div>";
			return $msg;
		} 
		else 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Sorry! There has been problem inserting your details.</div>";
			return $msg;
		}
	}

	public function getLoginUser($email, $password) {
		$sql = "SELECT * FROM user WHERE email = :email AND password = :password LIMIT 1";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':email', $email);
		$query->bindValue(':password', $password);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	public function userLogin($data)
	{
		$email 		= $data['email'];
		$password 	= $data['password'];

		if ($email == "" OR $password == "") 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Field must not be empty!</div>";
			return $msg;
		}

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Email address is not valid!</div>";
			return $msg;
		}

		if(strlen($password) < 4) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Password must not less than 4 digits!</div>";
			return $msg;
		}

		
		$chk_email	= $this->emailCheck($email);

		if ($chk_email == false) 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Email address not exists!</div>";
			return $msg;
		}

		//if(preg_match('/[^a-z0-9]+/i', $password))
		if (ctype_alnum($password))
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Password must be alphanumerical, dashes and underscore!!</div>";
			return $msg;
		}

		$password = md5($password);

		$result = $this->getLoginUser($email, $password);

		if ($result) {
			Session::init();
			Session::set("login", true);
			Session::set("id", $result->user_id);
			Session::set("username", $result->username);
			Session::set("email", $result->email);
			Session::set("role", $result->role);
			Session::set("location", $result->location);
			Session::set("interest", $result->interest);
			Session::set("loginmsg", "<div class='alert alert-success alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>You are loggedIn!</div>");

			if(Session::get('participateEventId') != "") {
				header('Location: congratulation.php');
			} 
			elseif(Session::get('participateEventId') == "") {
				header("Location: index.php");
			}
		} 
		else 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Data not found!</div>";
			return $msg;
		}
	}

	public function updateUser($data)
	{
		$username = $data['username'];
		$location = $data['location'];
		$interest = $data['interest'];

		//echo $username . " " . $location . " " . $interest;
		if ($username == "" OR $location == "" OR $interest == "") 
		{
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Fields must not be empty!</div>";
			return $msg;
		}

		$user_id = Session::get('id');

		$sql = "UPDATE user SET 
					username 	= :username,
					location 	= :location,
					interest 	= :interest
				WHERE user_id 	= :user_id";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':username', $username);
		$query->bindValue(':location', $location);
		$query->bindValue(':interest', $interest);
		$query->bindValue(':user_id', $user_id);
		$result = $query->execute();
		if ($result) {
			$msg = "<div class='alert alert-success alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success! </strong>Userdata updated Successfully!</div>";
			return $msg;
		} else {
			$msg = "<div class='alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error! </strong>Userdata Not Updated Yet!</div>";
			return $msg;
		}

	}

	public function getUserById($user_id) {
		$sql = "SELECT * FROM user WHERE user_id = :user_id LIMIT 1;";
		$query = $this->db->pdo->prepare($sql);
		$query->bindValue(':user_id', $user_id);
		$query->execute();
		//$result = $query->fetch(PDO::FETCH_OBJ);
		$result = $query->fetchAll();
		return $result;
	}


	


}